﻿using System;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            using var rabbitClient = new RabbitMQClient();
            Console.WriteLine("Pinger started!");
            rabbitClient.Start();
            Console.ReadKey();
        }
    }
}
