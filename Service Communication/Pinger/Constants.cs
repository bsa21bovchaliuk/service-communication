﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinger
{
    public static class Constant
    {
        public const int Delay = 2500;
        public const string ExchangeName = "ClientExchange";
        public const string ListenQueueKey = "ping_key";
        public const string ListenQueueName = "ping_queue";
        public const string Message = "ping";
        public const string WriteQueueKey = "pong_key";
        public const string WriteQueueName = "pong_queue";

    }
}
