﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ponger
{
    public static class Constant
    {
        public const int Delay = 2500;
        public const string ExchangeName = "ClientExchange";
        public const string ListenQueueKey = "pong_key";
        public const string ListenQueueName = "pong_queue";
        public const string Message = "pong";
        public const string WriteQueueKey = "ping_key";
        public const string WriteQueueName = "ping_queue";

    }
}
