﻿using System;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            using var rabbitClient = new RabbitMQClient();
            Console.WriteLine("Ponger started");
            rabbitClient.Start();
            Console.ReadKey();
        }
    }
}
