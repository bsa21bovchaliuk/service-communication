﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ponger
{
    public class RabbitMQClient : IDisposable
    {
        private readonly MessageService _msgService;

        public RabbitMQClient()
        {
            _msgService = new MessageService();
        }

        public void Start()
        {
            StartWriteToQueue();
            ListenQueue();
        }

        public void StartWriteToQueue()
        {
            _msgService.StartWriteToQueue(
                    Constant.ExchangeName,
                    ExchangeType.Direct,
                    Constant.WriteQueueName,
                    Constant.WriteQueueKey
                    );
        }

        public void ListenQueue()
        {
            _msgService.ListenQueue(
                    Constant.ExchangeName,
                    ExchangeType.Direct,
                    Constant.ListenQueueName,
                    Constant.ListenQueueKey,
                    MessageReceivedAsync);
        }

        private async void MessageReceivedAsync(object sender, BasicDeliverEventArgs args)
        {
            this.ListenQueue();
            await Task.Delay(Constant.Delay);
            _msgService.SendMessageToQueue(Constant.Message);
        }

        public void Dispose()
        {
            _msgService.Dispose();
        }
    }
}
