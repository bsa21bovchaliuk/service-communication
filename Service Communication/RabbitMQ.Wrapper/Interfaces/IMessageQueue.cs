﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageQueue : IDisposable
    {
        IModel Channel { get; }

        void DeclareExchange(string exchangeName, string exchangeType);

        void BindQueue(string exchangeName, string routingKey, string queueName);
    }
}
